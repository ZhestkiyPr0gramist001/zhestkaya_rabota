package com.example.zhestko.model

data class LoginData(
    val email : String,
    val password : String
)
