package com.example.zhestko.model

data class UserData(
    val token: String,
    val user: User
)

