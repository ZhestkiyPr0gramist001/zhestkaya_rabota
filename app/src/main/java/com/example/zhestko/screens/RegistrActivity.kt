package com.example.zhestko.screens

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.example.zhestko.R
import com.example.zhestko.common.API
import com.example.zhestko.common.Utils
import com.example.zhestko.model.RegisterData
import com.example.zhestko.model.UserData
import okhttp3.internal.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

class RegistrActivity : AppCompatActivity() {
    //Пол для выпадающего списка

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registr)

        val spinner = findViewById<Spinner>(R.id.spinner)
        val back = findViewById<TextView>(R.id.back)
        val registration = findViewById<Button>(R.id.registration)
        var sex = "Мужчина"

        val lastname = findViewById<EditText>(R.id.textView13)
        val firstname = findViewById<EditText>(R.id.textView11)
        val patronymic = findViewById<EditText>(R.id.textView19)
        val birthdate = findViewById<EditText>(R.id.textView23)
        val email = findViewById<EditText>(R.id.textView31)
        val password = findViewById<EditText>(R.id.textView33)
        val reppassword = findViewById<EditText>(R.id.textView25)

        back.setOnClickListener{
            startActivity(Intent(this@RegistrActivity, AuthActivity::class.java))
        }

        spinner.onItemSelectedListener = object:OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2==0){
                    sex = "Мужчина"
                }
                else{
                    sex = "Женщина"
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
        registration.setOnClickListener{
            if(lastname.text.toString() == "" || firstname.text.toString() == "" || patronymic.text.toString() == "" || birthdate.text.toString() == "" ||
            email.text.toString() == "" || password.text.toString() == "" || reppassword.text.toString() == ""){
                Utils.showAlertDialog(this,"Ошибка", "Проверьте поля на пустоту", "Повторить")
            }

            else if (password.text.toString() != reppassword.text.toString()){
                Utils.showAlertDialog(this, "Ошибка", "Пароли не совападают", "Повторить")
            }

            else if (!Utils.validateEmail(email.text.toString())){
                Utils.showAlertDialog(this, "Ошибка", "Проверьте правильность вашей пчоты", "Повторить")
            }

//            birthdate.setOnClickListener{
//                val c = Calendar.getInstance()
//                val year = c.get(Calendar.YEAR)
//                val month = c.get(Calendar.MONTH)
//                val day = c.get(Calendar.DAY_OF_MONTH)
//
//            }

            val retrofit = Retrofit.Builder()
                .baseUrl("http://95.31.130.149:8085/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val api = retrofit.create(API::class.java)

            api.registr(RegisterData(birthdate.text.toString(), email.text.toString(), firstname.text.toString(),
                lastname.text.toString(), password.text.toString(), patronymic.text.toString(), sex))
                .enqueue(object : Callback<UserData>{
                    override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                        if(response.isSuccessful){
                            val ed = Utils.getSharedPreferences(this@RegistrActivity).edit()
                            ed.putString("token", response.body()!!.token)
                            ed.putString("email", email.text.toString())
                            ed.putString("password", password.text.toString())
                            ed.commit()
                        }
                        else{
                            Utils.showAlertDialog(this@RegistrActivity,
                            "Ошибка",
                            "Такой пользователь уже существует",
                            "Попробовать снова")
                        }


                    }

                    override fun onFailure(call: Call<UserData>, t: Throwable) {
                        Utils.showAlertDialog(this@RegistrActivity,
                            "Ошибка",
                            t.message!!,
                            "Попробовать снова")
                    }
                })

        }
    }
}