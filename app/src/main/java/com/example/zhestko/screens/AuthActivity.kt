package com.example.zhestko.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.zhestko.R
import com.example.zhestko.common.API
import com.example.zhestko.common.Utils
import com.example.zhestko.model.LoginData
import com.example.zhestko.model.UserData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        val vhod = findViewById<Button>(R.id.registration)
        val reg = findViewById<TextView>(R.id.back)
        val email = findViewById<EditText>(R.id.email)
        val password = findViewById<EditText>(R.id.password)

        //При нажатии на авторизацию
        vhod.setOnClickListener{
            //Проверка
            if (password.text.toString() == "" || email.text.toString() == "") {
                Utils.showAlertDialog(
                    this,
                    "Ошибка",
                    "Проверьте поля на пустоту",
                    "Попробовать снова"
                )
            }

            else if (!Utils.validateEmail(email.text.toString())) {
                Utils.showAlertDialog(
                    this,
                    "Ошибка",
                    "Проверьте правильность вашего email",
                    "Попробовать снова"
                )
            }

            //Подготовка к запросу
            val retrofit = Retrofit.Builder()
                .baseUrl("http://95.31.130.149:8085/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val api = retrofit.create(API::class.java)

            //Запрос
            api.auth(LoginData(email.text.toString(), password.text.toString()))
                .enqueue(object : Callback<UserData> {
                    override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                        if (response.isSuccessful) {
                            //Сохранение данных
                            val ed = Utils.getSharedPreferences(this@AuthActivity).edit()
                            ed.putString("token", response.body()!!.token)
                            ed.putString("email", email.text.toString())
                            ed.putString("password", password.text.toString())
                            ed.commit()
                            startActivity(Intent(this@AuthActivity, MainActivity::class.java))
                        } else {
                            Utils.showAlertDialog(
                                this@AuthActivity,
                                "Ошибка",
                                response.message(),
                                "Попробовать снова"
                            )
                        }
                    }

                    override fun onFailure(call: Call<UserData>, t: Throwable) {
                        Utils.showAlertDialog(
                            this@AuthActivity,
                            "Ошибка",
                            t.message!!,
                            "Попробовать снова"
                        )
                    }
                })
        }

        //При нажатии на кнопку регистрации
        reg.setOnClickListener{
            startActivity(Intent(this, RegistrActivity::class.java))
        }
    }
}