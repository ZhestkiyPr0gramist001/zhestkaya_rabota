package com.example.zhestko.common

import com.example.zhestko.model.LoginData
import com.example.zhestko.model.RegisterData
import com.example.zhestko.model.UserData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface API {
    @POST("sigIn")
    fun auth(@Body data: LoginData) : Call<UserData>

    @POST("sigUp")
    fun registr(@Body data: RegisterData) : Call<UserData>
}