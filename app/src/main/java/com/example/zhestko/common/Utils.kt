package com.example.zhestko.common

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences

class Utils{
    companion object{
        fun validateEmail(email: String): Boolean{
            var cnt = -1
            var correct = false
            for (i in email){
                if(cnt != -1)
                    cnt++
                if(i == '@') {
                    if (correct) {
                        correct = false
                        break
                    }
                    correct = true
                }
                else if(i == '.') {
                    cnt = 0
                }else if(i in 'a'..'z'){
                }else if(cnt == -1 && i in '0'..'9'){}else
                {
                        correct = false
                        break
                    }
                }
            return correct && cnt<=3 && cnt>=1
            }

            public fun showAlertDialog(context: Context, title: String, message: String, button: String) {
                AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setMessage(message)
                    .setPositiveButton(button){dialogInterface: DialogInterface, i: Int -> }
                    .show()
            }

            fun getSharedPreferences(context: Context): SharedPreferences {
                return context.getSharedPreferences("Example1", 0)
        }
    }
}